package blog;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SubscribeServlet extends HttpServlet
{
	
	private static final Logger log = Logger.getLogger(SubscribeServlet.class.getName());
	
	static {
	    ObjectifyService.register(Subscriber.class);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)

	throws IOException
	{
		log.info("subscribeservlet");
		String email = req.getParameter("email");
		String unsubscribe = req.getParameter("unsubscribe");
		
		if (unsubscribe != null) {
			findUnsubscriber(email);
		} else {
			Subscriber subscriber = new Subscriber(email);
			ofy().save().entity(subscriber);
		}

		resp.sendRedirect("/homepage.jsp");
	}
	
	private void testSubscribers() {
		Query<Subscriber> subs = ofy().load().type(Subscriber.class);
		
		for (Subscriber sub : subs) {
			System.out.println(sub.getEmail());
		}
		System.out.println();
	}
	
	//deletes person who has unsubscribed
	private void findUnsubscriber(String email) {
		Query<Subscriber> subs = ofy().load().type(Subscriber.class);
		
		for (Subscriber sub : subs) {
			String em = sub.getEmail();
			if (em.equals(email)) {
				ofy().delete().entity(sub);
			}
		}
	}
}

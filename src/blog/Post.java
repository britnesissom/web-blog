package blog;
import java.util.Date;
 
import com.google.appengine.api.users.User;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
 
 
@Entity
public class Post implements Comparable<Post> {
    @Id Long id;
    User user;
    String title;
    String content;
    Date date;
    long time;
    
    private Post() {}
    
    public Post(User user, String title, String content) {
        this.user = user;
        this.title = title;
        this.content = content;
        date = new Date();
        time = date.getTime();
    }
    
    public User getUser() {
        return user;
    }
    
    public String getContent() {
        return content;
    }
    
    public String getTitle() {
        return title;
    }
    
    public Date getDate() {
        return date;
    }
    
    public long getTime() {
        return time;
    }
    
    public boolean compareDates(long postDate, long cronDate) {
    	if (postDate > cronDate)
    		return true;
    	return false;
    }
    
    @Override
    public int compareTo(Post other) {
        if (date.after(other.date)) {
            return 1;
        } else if (date.before(other.date)) {
            return -1;
        }
        return 0;
    }
}
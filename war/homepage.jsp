<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="blog.Post" %>
<%@ page import="com.googlecode.objectify.Objectify" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.TimeZone" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
 

<html>
 <head>
   <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
   <link type="text/css" rel="stylesheet" href="/stylesheets/main.css" />
 </head>

  <body>

 
<div id="sidebar">
	<div id="sidecontent">
<%

    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
      pageContext.setAttribute("user", user);

%>

		<p>Hello, ${fn:escapeXml(user.nickname)}! (Sign out
		<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">here</a>.)</p>
		
		<%
		    } else {
		%>
		
		<p>Hello!
		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
		to post.</p>
		
		<%
		    }
		%>
		
		  <form action="/subscribe" method="post">
		  <div><textarea name="email" rows="1" cols="30" placeholder="Enter email to (un)subscribe"></textarea></div>
		      <div><input type="submit" name="subscribe" value="Subscribe" />
		      <input type="submit" name="unsubscribe" value="Unsubscribe" /></div>
		      <input type="hidden" name="blogName" value="${fn:escapeXml(blogName)}"/>
		  </form>
		  
		  <br>
		  <div id="header"><img src="http://media3.s-nbcnews.com/j/msnbc/Components/Photo/_new/081016-catshow-hmed-12p.grid-6x2.jpg"></div>
		
			<div id="links">
			<br><a href="homepage.jsp">Home</a>
			<br>
		
		<% 
		
				if (user != null) {
		%>
					<a href="createpost.jsp">Create new post</a>
		<%
				}
		%>
		
			<br><a href="allposts.jsp">View all posts</a>
			</div>
			
			<div id="sitetitle">
  				<h1>The Real Winners</h1>
  			</div>
	</div>
</div>

 	<%
    String blogName = request.getParameter("blogName");
    if (blogName == null) {
        blogName = "default";
    }
    
    pageContext.setAttribute("blogName", blogName);

	ObjectifyService.register(Post.class);
	List<Post> posts = ObjectifyService.ofy().load().type(Post.class).list();
	Collections.sort(posts);
	Collections.reverse(posts);
    
    if (posts.isEmpty()) {
        %>
        <h1>There are no posts.</h1>
        <%
    } else {
        %>
        <h1>Recent Posts</h1>
        <%

        Integer size = 0;
        if(posts.size() < 5){
        	size = posts.size();
        } else { 
        	size = 5;
        }
        
        List<Post> sublist = posts.subList(0,size.intValue());
        for (Post post: sublist) {
    
            pageContext.setAttribute("post_content", post.getContent());
            pageContext.setAttribute("post_title", post.getTitle());
            pageContext.setAttribute("post_user", post.getUser());
            
            DateFormat formatter = new SimpleDateFormat("EEE, MMM dd, yyyy 'at' HH:mm a z");
        		Date fromDate = post.getDate();
        		TimeZone central = TimeZone.getTimeZone("America/Chicago");
        		formatter.setTimeZone(central);
            pageContext.setAttribute("post_date", formatter.format(fromDate));
                                         
         %>
				
	         	 <div class="post">
	         	 <h3>${fn:escapeXml(post_title)}</h3>
	         	 <div id="content">${fn:escapeXml(post_content)}</div>
	         	 <div id="postinfo">
		         	 <div id="poster">Posted by <b>${fn:escapeXml(post_user.nickname)}</b></div>
		         	 <br> 
		         	 ${fn:escapeXml(post_date)}
	         	 </div>
	         	 </div>
         <%
         }
         %>
                
         
      <%
   	}
      %>

  </body>

</html>
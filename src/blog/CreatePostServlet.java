package blog;

import static com.googlecode.objectify.ObjectifyService.ofy;
import blog.Post;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

public class CreatePostServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(CreatePostServlet.class.getName());

    //must register classes in a static initializer before Objectify is used
	static {
	    ObjectifyService.register(Post.class);
	}

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
 
       //get the content and title from the request
        String content = req.getParameter("content");
        String title = req.getParameter("title");
        
        //create a new Post using the user and content
        Post post = new Post(user, title, content);
        
        //chuck the Post into Objectify using a synchronous call
        ofy().save().entity(post);

        log.info("Post created");
        //Send the response
		resp.sendRedirect("/homepage.jsp");
	}
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException 
	{
    	UserService userService = UserServiceFactory.getUserService();
    	User user = userService.getCurrentUser();
    	
    	if (user == null) {
    		resp.sendRedirect(userService.createLoginURL("/homepage.jsp"));
    		//resp.sendRedirect("/homepage.jsp");
    	}
	}
}
package blog;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

@SuppressWarnings("serial")
public class CronServlet extends HttpServlet {
	private static final Logger _logger = Logger.getLogger(CronServlet.class
			.getName());
	
	static {
		ObjectifyService.register(Post.class);
		ObjectifyService.register(Subscriber.class);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		//gets list of all posts then sees if there have been any posts made in the time limit
		Date d = new Date();
		long currentTime = d.getTime();
		long compareTimeTo = 1000 * 60 * 60 * 24;	//ms * s * min * hr * day
		long time = currentTime - compareTimeTo;

		String strCallResult = "";
		Query<Post> posts = ObjectifyService.ofy().load().type(Post.class);
		for (Post post : posts) {
			//only send email if there are new posts
			if (withinTimeLimit(post, time)) {
				strCallResult = formEmailContent(post, time, strCallResult);
			}
		}

		int numSubs = ofy().load().type(Subscriber.class).list().size();
		
		if (strCallResult.isEmpty() == false && numSubs != 0) {
			sendEmail(strCallResult);
			_logger.info("email sent");
			strCallResult = "";
		} else {
			_logger.info("no email sent");
			strCallResult = "";
		}
		
	}
	
	private boolean withinTimeLimit(Post post, long time) {
		return post.getTime() > time;
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
	
	public String formEmailContent(Post post, long date, String strCallResult) {
		_logger.info("there is a post!!");
		DateFormat formatter = new SimpleDateFormat("EEE, MMM dd, yyyy 'at' HH:mm a z");
		Date fromDate = post.getDate();
		TimeZone central = TimeZone.getTimeZone("America/Chicago");
		formatter.setTimeZone(central);
        String postDate = formatter.format(fromDate);
        
		strCallResult += "Posted by " + post.getUser() + " on: " + postDate + "\n";
		strCallResult += "Title: " + post.getTitle() + "\n";
		strCallResult += "Content: " + post.getContent() + "\n";
		strCallResult += "----------------------";
		strCallResult += "\n\n";
		_logger.info(strCallResult);
		
		return strCallResult;
	}
	
	public void sendEmail(String emailContent) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		
		//get list of subscribers to send emails to
		Query<Subscriber> subs = ofy().load().type(Subscriber.class);
		
		for (Subscriber sub : subs) {
			String em = sub.getEmail();
			InternetAddress newEmail;
			try {
				newEmail = new InternetAddress(em, true);
				MimeMessage outMessage = new MimeMessage(session);
				outMessage.setFrom(new InternetAddress("admin@britnesandrobinsblog.appspotmail.com"));
				outMessage.addRecipient(MimeMessage.RecipientType.TO, newEmail);
				outMessage.setSubject("New Posts");
				outMessage.setText(emailContent);
				Transport.send(outMessage);
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}
	
//	private boolean timeLimitCheck() {
//		int day = Calendar.DAY_OF_YEAR;
//		int hour = Calendar.HOUR_OF_DAY;
//		int minute = Calendar.MINUTE;
//		int millisecond = Calendar.MILLISECOND;
//		return true;
//	}
}

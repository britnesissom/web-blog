<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="blog.Post" %>
<%@ page import="com.googlecode.objectify.Objectify" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
 <head>
   <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
   <link type="text/css" rel="stylesheet" href="/stylesheets/main.css" />
 </head>

  <body>
  <div id="sidebar">
	<div id="sidecontent">
<%

    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
      pageContext.setAttribute("user", user);

%>
		<p>Hello, ${fn:escapeXml(user.nickname)}! (Sign out
		<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">here</a>.)</p>
	
		<%
		    } else {
		%>
		
		<p>Hello!
		<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
		to post.</p>
		
		
		<%
		    }
		%>
		
		 <form action="/subscribe" method="post">
		  <div><textarea name="email" rows="1" cols="30" placeholder="Enter email to (un)subscribe"></textarea></div>
		      <div><input type="submit" name="subscribe" value="Subscribe" />
		      <input type="submit" name="unsubscribe" value="Unsubscribe" /></div>
		      <input type="hidden" name="blogName" value="${fn:escapeXml(blogName)}"/>
		  </form>
		  <br><img src="http://media3.s-nbcnews.com/j/msnbc/Components/Photo/_new/081016-catshow-hmed-12p.grid-6x2.jpg">
		 
		<div id="links">
		   <br><a href="homepage.jsp">Home</a>
			<br><a href="allposts.jsp">View all posts</a>
		</div>
		
		<div id="sitetitle">
  				<h1>The Real Winners</h1>
  		</div>
	
	</div>
</div>
  
  <div id="createtitle">
		<h1> Create a Post </h1>
  </div>
  
  <div id="postcreator">
		<form action="/createpost" method="post">
			<div><textarea name="title" rows="1" cols="60" placeholder="Post title goes here"></textarea></div>
		   <div><textarea name="content" rows="5" cols="60" placeholder="Write your post content here"></textarea></div>
	  
	      <input type="submit" value="Submit" />
	      <input type="hidden" name="blogName" value="${fn:escapeXml(blogName)}"/>
	    </form>
	    
	   	<form action="/homepage.jsp" method="get">
	      <input type="submit" value="Cancel" />
	    </form>
    </div>
	
	<%
    if (user == null) {
      pageContext.setAttribute("user", user);
	%>
		<div id="createtitle">
			<h2>Oh, you trickster. You MUST sign in to create a post.</h2>
		</div>
	<%
	}
	%>

		
  </body>
  
</html>


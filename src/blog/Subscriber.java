package blog;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Subscriber {
    @Id Long id;
    String email = "";
    
    private Subscriber() {}
    
    public Subscriber(String email) {
        this.email += email;
    }
	
	public String getEmail() {
		return email;
	}
}
